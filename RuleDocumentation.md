## Description:
“Tetris but 2048” is a game where players aim to merge numbered blocks and score high before there is no space to fit any new blocks. The game features a playing field where numbered blocks will fall at regular intervals.
The player can move the blocks horizontally using arrow keys and rotate using a rotate button. When two or more blocks with the same number touch vertically, they will merge into one block with a value equal to the sum of the merged blocks. The game includes many types of blocks with different values and colors.
The game features two difficulty levels - normal and difficult - that differ in block falling speed, which players can adjust in settings. The game ends when the blocks reach the top of the screen and there is no more space to add new blocks.
Players earn points based on the sum of the values of blocks in the playing field. The game includes a score that displays at the top of the game page.

## Min. Requirement:

1. Players can move the blocks left or right using the arrow keys and rotate them using the up arrow key. The blocks will fall automatically, but players can pause the game by pressing the pause button.
2. The blocks will fall automatically once the game begins. Users will have the ability to control some of the moving pieces through input. We provide the user with four buttons to control the moving blocks: left, right, rotate, and pause.
3. The game includes a score that changes based on the user's ability to satisfy requirements. There's a score displayed at the top of the game page that shows the points the user earns by summing up the values of blocks in the playing field.
4. The game features a simple-to-use graphical user interface. All buttons and texts are clear and easy to see and use.
5. We also provide clear documentation of the game's rules in the README file.
6. The game does not require any Internet or network connectivity, as it can be played locally. It does not violate the copyright of Tetris or any other game. While our game is inspired by Tetris and 2048, it is fundamentally different from them.
## Potential Feature have been done:
1. Allow the user to interact with the game by moving the phone:
When you are shaking your phone, the following block will rotate once. 
Provide a high-score list that persists when the app is closed and then reopened: when the game is done, it will jump to a new page tells your score and then it will show you your rank and the 3 highest score the play have got.  
2. Allow the user to tweak the rules of the game being played: In the set, you can adjust the difficulty of the game. Blocks will fall faster in difficult mode. 
3. Add sound effects for specific activities in the game: sound will be played when you click the button or shake phone to rotate block or when blocks are merged. 
4. Do all calculations in C++ and connect them to the app through Android's Native Development Kit: the function of sorting score and calculate the score in the game are wrote in C++.  


## Design decisions:
1. Pause game - We used a timer to implement the pause function. When the pause button is pressed, the timer is stopped, which means that the game will stop updating and the blocks will stop falling. This creates the effect of pausing the game. When the pause button is pressed again, the timer resumes, which means that the game will continue updating and the blocks will start falling again.
2. Rotate Block when shaking the phone -  Shaking the phone will causes the blocks to rotate once. To achieve this, we created a variable that records whether the block has already rotated due to shaking and refreshes it every time cycle to ensure the block rotates only once per time circle.
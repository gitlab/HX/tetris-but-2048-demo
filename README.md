# Group9Project DEMO

## Project Name:
Tetris but 2048
## Category: 
Tetris-like Animated game. 

## Description:
Combine the fast-paced falling block mechanics of Tetris with the number-merging gameplay of 2048. Merge blocks, create bigger numbers, and score high！

## How to run our game:
1. Clone our code with the Link:https://agile.bu.edu/gitlab/HX/tetris-but-2048-demo.git
2. Open the folder you have downloaded in Android Studio.
3. Click Device Manager, and click Create device. We recommend you to choose Category: Phone, Name : Pixel 2, API Level: 30. (API Level must be larger than 24)   
4. When everything is ready, click the green triangle at the top right toolbar. 

## Game Rule Documentation:
Objective:

The objective of “Tetris but 2048” is to score as many points as possible by merging numbered blocks before they reach the top of the playing field.

Gameplay:

The game consists of a playing field in which numbered blocks will fall from the top at regular intervals.
The player can move the blocks horizontally using arrow keys and rotate using a rotate button.
When two or more blocks with the same number touch vertically, they will merge into one block with a value equal to the sum of the merged blocks
The game ends when the blocks reach the top of the screen and there is no more space to add new blocks.
The game includes many types of blocks with different values and colors.
The game includes two difficulty levels - normal and difficult - differ in block falling speed, player can adjust it in setting

Scoring:

The player earns points for the sum of the values of blocks in the playing grid.

Rules:

The blocks can be moved left or right using the arrow keys and rotated using the up arrow key.
The blocks will fall automatically and can be paused by pressing the pause button.


## Group Member: 
Houjie Xiong, Ruohui Huang, Haochen Sun, Yangyang Zhang, Shi Gu


## Video Link:
https://drive.google.com/file/d/1CdC7egppOd1ArcsQGnYXn1ezkJy-MvIk/view?usp=share_link

package com.example.tetrisbut2048;

import androidx.appcompat.app.AppCompatActivity;

import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

public class Setting extends Rule {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        SoundPool soundPool = new SoundPool.Builder().build();
        int buttonSound = soundPool.load(this, R.raw.button_sound, 1);
        Button button_back = (Button) findViewById(R.id.button_back_set);
        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                openMain();
            }
        });

        Switch switchSpeed = findViewById(R.id.switch_set1);
        switchSpeed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Handle the toggle event
                if (isChecked) {
                    // Do something when switch is on
                    Game.period = 1000;
                    switchSpeed.setText("Fast");
                } else {
                    // Do something when switch is off
                    Game.period = 1500;
                    switchSpeed.setText("Slow");
                }
            }

        });
    }

}
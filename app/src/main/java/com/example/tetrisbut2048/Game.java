package com.example.tetrisbut2048;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Timer;
import java.util.TimerTask;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.hardware.SensorEventListener;


public class Game extends AppCompatActivity {
    private Timer timer;
    private Context context;
    private boolean paused = false;
    private boolean moving=false;
    private int score=0;
    private TextView[] BlockArray= new TextView[50];
    private int leftBlock = 0;
    private int rightBlock = 0;
    private int nextNum1=0;
    private int nextNum2=0;
    private int currNum1=0;
    private int currNum2=0;
    private TextView next1;
    private TextView next2;
    private int posture=0;
    private boolean beginning=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_game);

        SoundPool soundPool = new SoundPool.Builder().build();
        int buttonSound = soundPool.load(this, R.raw.button_sound, 1);
        int rotate_block = soundPool.load(this, R.raw.rotate_block, 1);



        TextView text_exit = (TextView) findViewById(R.id.text_exit);
        text_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                openMain();
            }
        });
        Button pauseButton = findViewById(R.id.pause_button);
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                pauseGame();
            }
        });
        Button rightButton = findViewById(R.id.MoveRight);
        rightButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                if (hitGround() || paused) {
                }
                else{
                    moveRight();
                }
            }
        });
        Button leftButton = findViewById(R.id.MoveLeft);
        leftButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                if (hitGround() || paused) {
                }
                else{
                    moveLeft();
                }
            }
        });
        Button rotateButton = findViewById(R.id.RotateClockwise);
        rotateButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPool.play(rotate_block, 1.0f, 1.0f, 0, 0, 1.0f);
                if (hitGround() || paused) {
                }
                else{
                    rotateClockwise();
                }
            }
        });
        SensorManager sensorManager=(SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensor=sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        SensorEvent sensorEvent;
        SensorEventListener sensorEventListener=new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if(event!=null){
                    double x_accl = event.values[0];
                    if (movePhoneEnable) {
                        if (x_accl >= 0.01) {
                            rotateClockwise();
                            soundPool.play(rotate_block, 1.0f, 1.0f, 0, 0, 1.0f);
                        } else if (x_accl <= -0.01) {
                            rotateClockwise();
                            soundPool.play(rotate_block, 1.0f, 1.0f, 0, 0, 1.0f);
                        }
                        movePhoneEnable=false;
                    }
                }
            }
            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };
        sensorManager.registerListener(sensorEventListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        create_blockArray();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!paused) {
            startTimer();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }
    public void create_blockArray(){
        next1=(TextView) findViewById(R.id.nextBlock1);
        next2=(TextView) findViewById(R.id.nextBlock2);

        BlockArray[0]=(TextView) findViewById(R.id.Block1);
        BlockArray[1]=(TextView) findViewById(R.id.Block2);
        BlockArray[2]=(TextView) findViewById(R.id.Block3);
        BlockArray[3]=(TextView) findViewById(R.id.Block4);
        BlockArray[4]=(TextView) findViewById(R.id.Block5);
        BlockArray[5]=(TextView) findViewById(R.id.Block6);
        BlockArray[6]=(TextView) findViewById(R.id.Block7);
        BlockArray[7]=(TextView) findViewById(R.id.Block8);
        BlockArray[8]=(TextView) findViewById(R.id.Block9);
        BlockArray[9]=(TextView) findViewById(R.id.Block10);
        BlockArray[10]=(TextView) findViewById(R.id.Block11);
        BlockArray[11]=(TextView) findViewById(R.id.Block12);
        BlockArray[12]=(TextView) findViewById(R.id.Block13);
        BlockArray[13]=(TextView) findViewById(R.id.Block14);
        BlockArray[14]=(TextView) findViewById(R.id.Block15);
        BlockArray[15]=(TextView) findViewById(R.id.Block16);
        BlockArray[16]=(TextView) findViewById(R.id.Block17);
        BlockArray[17]=(TextView) findViewById(R.id.Block18);
        BlockArray[18]=(TextView) findViewById(R.id.Block19);
        BlockArray[19]=(TextView) findViewById(R.id.Block20);
        BlockArray[20]=(TextView) findViewById(R.id.Block21);
        BlockArray[21]=(TextView) findViewById(R.id.Block22);
        BlockArray[22]=(TextView) findViewById(R.id.Block23);
        BlockArray[23]=(TextView) findViewById(R.id.Block24);
        BlockArray[24]=(TextView) findViewById(R.id.Block25);
        BlockArray[25]=(TextView) findViewById(R.id.Block26);
        BlockArray[26]=(TextView) findViewById(R.id.Block27);
        BlockArray[27]=(TextView) findViewById(R.id.Block28);
        BlockArray[28]=(TextView) findViewById(R.id.Block29);
        BlockArray[29]=(TextView) findViewById(R.id.Block30);
        BlockArray[30]=(TextView) findViewById(R.id.Block31);
        BlockArray[31]=(TextView) findViewById(R.id.Block32);
        BlockArray[32]=(TextView) findViewById(R.id.Block33);
        BlockArray[33]=(TextView) findViewById(R.id.Block34);
        BlockArray[34]=(TextView) findViewById(R.id.Block35);
        BlockArray[35]=(TextView) findViewById(R.id.Block36);
        BlockArray[36]=(TextView) findViewById(R.id.Block37);
        BlockArray[37]=(TextView) findViewById(R.id.Block38);
        BlockArray[38]=(TextView) findViewById(R.id.Block39);
        BlockArray[39]=(TextView) findViewById(R.id.Block40);
        BlockArray[40]=(TextView) findViewById(R.id.Block41);
        BlockArray[41]=(TextView) findViewById(R.id.Block42);
        BlockArray[42]=(TextView) findViewById(R.id.Block43);
        BlockArray[43]=(TextView) findViewById(R.id.Block44);
        BlockArray[44]=(TextView) findViewById(R.id.Block45);
        BlockArray[45]=(TextView) findViewById(R.id.Block46);
        BlockArray[46]=(TextView) findViewById(R.id.Block47);
        BlockArray[47]=(TextView) findViewById(R.id.Block48);
        BlockArray[48]=(TextView) findViewById(R.id.nextBlock1);
        BlockArray[49]=(TextView) findViewById(R.id.nextBlock2);
    }
    private void next_blocks(){
        int max=5;
        int min=1;
        double random1= (int)Math.floor(Math.random()*(max-min+1))+1;
        double random2= (int)Math.floor(Math.random()*(max-min+1))+1;
        nextNum1 = (int) Math.pow(2,random1);//num1 and num2 is a random number from {2 4 8 16 32}
        nextNum2 = (int) Math.pow(2,random2);
        setBlockNumNCol(48,nextNum1);
        setBlockNumNCol(49,nextNum2);

    }
    private void fallingBlocks(){
        leftBlock=2;
        rightBlock=3;
        currNum1=nextNum1;
        currNum2=nextNum2;
        setBlockNumNCol(leftBlock,Integer.parseInt(next1.getText().toString()));
        setBlockNumNCol(rightBlock,Integer.parseInt(next2.getText().toString()));
        moving=true;
        posture=0;
//        score+=currNum1;
//        score+=currNum2;
    }
    private boolean ifGameOver() {
        if (BlockArray[2].getText().toString() != "" || BlockArray[3].getText().toString() != "") {
            return true;
        }
        return false;
    }
    private void setBlockNumNCol(int index, int num){
        switch(num){
            case 0:
                BlockArray[index].setBackgroundResource(R.drawable.block);//intend to return back to normal color
                BlockArray[index].setText("");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 2:
                BlockArray[index].setBackgroundResource(R.drawable.block2);
                BlockArray[index].setText("2");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 4:
                BlockArray[index].setBackgroundResource(R.drawable.block4);
                BlockArray[index].setText("4");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 8:
                BlockArray[index].setBackgroundResource(R.drawable.block8);
                BlockArray[index].setText("8");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 16:
                BlockArray[index].setBackgroundResource(R.drawable.block16);
                BlockArray[index].setText("16");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 32:
                BlockArray[index].setBackgroundResource(R.drawable.block32);
                BlockArray[index].setText("32");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 64:
                BlockArray[index].setBackgroundResource(R.drawable.block64);
                BlockArray[index].setText("64");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 128:
                BlockArray[index].setBackgroundResource(R.drawable.block128);
                BlockArray[index].setText("128");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 256:
                BlockArray[index].setBackgroundResource(R.drawable.block256);
                BlockArray[index].setText("256");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 512:
                BlockArray[index].setBackgroundResource(R.drawable.block512);
                BlockArray[index].setText("512");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 1024:
                BlockArray[index].setBackgroundResource(R.drawable.block1024);
                BlockArray[index].setText("1024");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            case 2048:
                BlockArray[index].setBackgroundResource(R.drawable.block2048);
                BlockArray[index].setText("2048");
                BlockArray[index].setTextColor(getColor(R.color.black));
                break;
            default:
                BlockArray[index].setBackgroundResource(R.drawable.block_black);
                BlockArray[index].setText(Integer.toString(num));
                BlockArray[index].setTextColor(getColor(R.color.white));
                break;
        }
    }
    private void fallOnece(){
        setBlockNumNCol(leftBlock,0);
        setBlockNumNCol(rightBlock,0);
        leftBlock+=6;
        rightBlock+=6;
        setBlockNumNCol(leftBlock,currNum1);
        setBlockNumNCol(rightBlock,currNum2);
    }
    private void moveBlock(int originLocation,int newLocation){
        int temp;
        if (BlockArray[originLocation].getText().toString() == "") {
            temp = 0;
        } else {
            temp = Integer.parseInt(BlockArray[originLocation].getText().toString());
        }
        setBlockNumNCol(newLocation,temp);
        setBlockNumNCol(originLocation,0);
    }
    private void moveRight(){
        if(leftBlock==5 || rightBlock==5 ||
                leftBlock==11 || rightBlock==11 ||
                leftBlock==17 || rightBlock==17 ||
                leftBlock==23 || rightBlock==23 ||
                leftBlock==29 || rightBlock==29 ||
                leftBlock==35 || rightBlock==35 ||
                leftBlock==41 || rightBlock==41 ||
                leftBlock==47 || rightBlock==47){

        } else if (checkHitRight()) {
        }
        else{
            if(leftBlock+1==rightBlock){
                moveBlock(rightBlock,rightBlock+1);
                moveBlock(leftBlock,leftBlock+1);
            }
            else if(rightBlock+1==leftBlock){
                moveBlock(leftBlock,leftBlock+1);
                moveBlock(rightBlock,rightBlock+1);
            }
            else{
                moveBlock(leftBlock,leftBlock+1);
                moveBlock(rightBlock,rightBlock+1);
            }
            leftBlock++;
            rightBlock++;
        }
    }
    private boolean checkHitRight(){
        if (leftBlock+1==rightBlock){
            if (BlockArray[rightBlock+1].getText()==""){
                return false;
            }
        }
        else if(rightBlock+1==leftBlock){
            if (BlockArray[leftBlock+1].getText()==""){
                return false;
            }
        }
        else{
            if (BlockArray[leftBlock+1].getText()==""){
                if (BlockArray[rightBlock+1].getText()==""){
                    return false;
                }
            }
        }
        return true;
    }
    private boolean checkHitLeft(){
        if (leftBlock+1==rightBlock){
            if (BlockArray[leftBlock-1].getText()==""){
                return false;
            }
        }
        else if(rightBlock+1==leftBlock){
            if (BlockArray[rightBlock-1].getText()==""){
                return false;
            }
        }
        else{
            if (BlockArray[leftBlock-1].getText()==""){
                if (BlockArray[rightBlock-1].getText()==""){
                    return false;
                }
            }
        }
        return true;
    }
    private void moveLeft(){
        if(leftBlock==0 || rightBlock==0 ||
                leftBlock==6 || rightBlock==6 ||
                leftBlock==12 || rightBlock==12 ||
                leftBlock==18 || rightBlock==18 ||
                leftBlock==24 || rightBlock==24 ||
                leftBlock==30 || rightBlock==30 ||
                leftBlock==36 || rightBlock==36 ||
                leftBlock==42 || rightBlock==42){

        } else if (checkHitLeft()) {
        }
        else{
            if(leftBlock+1==rightBlock){
                moveBlock(leftBlock,leftBlock-1);
                moveBlock(rightBlock,rightBlock-1);
            }
            else if(rightBlock+1==leftBlock){
                moveBlock(rightBlock,rightBlock-1);
                moveBlock(leftBlock,leftBlock-1);
            }
            else{
                moveBlock(rightBlock,rightBlock-1);
                moveBlock(leftBlock,leftBlock-1);
            }
            leftBlock--;
            rightBlock--;
        }
    }
    private void clearLine(){};
    private void mergeBlock(){
        TextView text_score = findViewById(R.id.text_score);
        score = addToScore(score,currNum1,currNum2);
        text_score.setText(Integer.toString(score));
        int mergeIndex=checkMerge();
        while (mergeIndex!=-1) {
            int result = 2 * Integer.parseInt(BlockArray[mergeIndex].getText().toString());
            setBlockNumNCol(mergeIndex, result);
            setBlockNumNCol(mergeIndex-6, 0);
            collumFall(mergeIndex - 6);
            if (posture == 0 || posture == 2) {
                if (leftBlock < 42)
                    if (BlockArray[leftBlock + 6].getText() == "") {
                        fallToGround(leftBlock);
                    } else if (BlockArray[rightBlock + 6].getText() == "") {
                        fallToGround(rightBlock);
                    }
            }
            mergeIndex = checkMerge();

        }
    }
    private void fallToGround(int index) {
        int ii=index;
        while (ii<42 && BlockArray[ii+6].getText()=="") {
            moveBlock(ii,ii+6);
            ii+=6;
        }
    }
    private void collumFall(int index){
        int ii=index;
        while (ii>5 && (BlockArray[ii-6].getText().toString())!=""){
            setBlockNumNCol(ii,Integer.parseInt(BlockArray[ii-6].getText().toString()));
            ii-=6;
        }
        setBlockNumNCol(ii,0);
    }
    private int checkMerge(){
        for(int ii=47;ii>=6;ii--){
            if(BlockArray[ii].getText()!="" && BlockArray[ii].getText()==BlockArray[ii-6].getText()){
                return ii;
            }
        }
        return -1;
    }
    public void openMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    public void openGameOver() {
        Intent intent = new Intent(this, GameOver.class);
        startActivity(intent);
    }
    private void pauseGame() {
        paused = !paused;
        Button pauseButton = findViewById(R.id.pause_button);
        if (paused) {
            pauseButton.setText("▶");
            timer.cancel();
            // Pause the game
            // Stop any ongoing animations or movement
        } else {
            pauseButton.setText("| |");
            startTimer();
            // Resume the game
            // Start any animations or movement again
        }
    }
    private  boolean hitGround(){
        if (leftBlock+6>47 ||rightBlock+6>47){
            return true;
        }
        else {
            if (posture == 1){
                if (BlockArray[rightBlock+6].getText()!="") {
                    return true;
                }
                return false;
            } else if (posture == 3 ){
                if(BlockArray[leftBlock+6].getText()!="") {
                    return true;
                }
                return false;
            }
            else if (BlockArray[leftBlock+6].getText()!="" || BlockArray[rightBlock+6].getText()!=""){
                return true;
            }
        }
        return false;
    }
    private void rotateClockwise(){
        if (checkCouldRotate()){
            switch (posture){
                case 0:
                    moveBlock(rightBlock,leftBlock+6);
                    rightBlock=leftBlock+6;
                    break;
                case 1:
                    moveBlock(rightBlock,leftBlock-1);
                    rightBlock=leftBlock-1;
                    break;
                case 2:
                    moveBlock(rightBlock,leftBlock-6);
                    rightBlock=leftBlock-6;
                    break;
                case 3:
                    moveBlock(rightBlock,leftBlock+1);
                    rightBlock=leftBlock+1;
                    break;
            }
            posture++;
        }
        if (posture==4){
            posture=0;
        }
    }
    private boolean checkCouldRotate(){
        switch (posture){
            case 0:
                if((BlockArray[leftBlock+6].getText().toString())!="" && leftBlock<42){
                    return false;
                }
                else{
                    return true;
                }
            case 1:
                if((BlockArray[leftBlock-1].getText().toString())!=""
                        || leftBlock==0  ||  leftBlock==6
                        || leftBlock==12  || leftBlock==18
                        || leftBlock==24  ||  leftBlock==30
                        || leftBlock==36  ||  leftBlock==42){
                    return false;
                }
                else{
                    return true;
                }
            case 2:
                if((BlockArray[leftBlock-6].getText().toString())!="" && leftBlock>5){
                    return false;
                }
                else{
                    return true;
                }
            case 3:
                if((BlockArray[leftBlock+1].getText().toString())!=""
                        || leftBlock==5  ||  leftBlock==11
                        || leftBlock==17  || leftBlock==23
                        || leftBlock==29  ||  leftBlock==35
                        || leftBlock==41  ||  leftBlock==47){
                    return false;
                }
                else{
                    return true;
                }
        }
        return true;
    }
    boolean movePhoneEnable=false;
    private void startTimer() {
        context = this;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        movePhoneEnable=true;

                        if (moving) {
                            if (hitGround()){
                                moving=false;
                                mergeBlock();
                                clearLine();
                            }
                            else{
                                fallOnece();
                            }
                        }
                        else{
                            if (beginning){
                                next_blocks();
                                beginning=false;
                            }
                            else{
                                if (ifGameOver()){
                                    timer.cancel();
                                    GameOver.finalScore=score;
                                    openGameOver();
                                } else {
                                    fallingBlocks();
                                    next_blocks();
                                }
                            }
                        }
                    }
                });
            }
        }, 0, period);
    }
    public static int period = 1500;
    public native int addToScore(int score, int block1, int block2);
}

package com.example.tetrisbut2048;

import androidx.appcompat.app.AppCompatActivity;

import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;


public class Score extends Setting {
    private SharedPreferences sharedPreferences;
    private ArrayList<String> scoreHistory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        sharedPreferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        scoreHistory = new ArrayList<>();
        String scoreHistoryString = sharedPreferences.getString("scoreHistory", "");
        if (!scoreHistoryString.isEmpty()) {
            String[] scoreHistoryArray = scoreHistoryString.split(",");
            scoreHistory.addAll(Arrays.asList(scoreHistoryArray));
        }
        scoreHistoryString = "";
        for (int i = 0; i < scoreHistory.size(); i++) {
            scoreHistoryString += scoreHistory.get(i);
            if (i < scoreHistory.size() - 1) {
                scoreHistoryString += "\n";
            }
        }

        TextView scoretext = (TextView) findViewById(R.id.textView3);
        scoretext.setText(scoreHistoryString);

        SoundPool soundPool = new SoundPool.Builder().build();
        int buttonSound = soundPool.load(this, R.raw.button_sound, 1);
        Button button_back = (Button) findViewById(R.id.button_back_score);
        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                openMain();
            }
        });
    }
}
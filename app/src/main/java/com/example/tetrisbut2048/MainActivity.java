package com.example.tetrisbut2048;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Timer;
import java.util.TimerTask;
public class MainActivity extends AppCompatActivity {

    // Used to load the 'tetrisbut2048' library on application startup.
    static {
        System.loadLibrary("tetrisbut2048");
    }
    private Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SoundPool soundPool = new SoundPool.Builder().build();
        int buttonSound = soundPool.load(this, R.raw.button_sound, 1);
        // Example of a call to a native method
        TextView tv = findViewById(R.id.game_title);
//        tv.setText(stringFromJNI());
        // Button Game
        Button button_game = (Button) findViewById(R.id.button_start);
        button_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                openGame();
            }
        });
        // Button Rule
        Button button_rule = (Button) findViewById(R.id.button_rule);
        button_rule.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                openRule();
            }

        });
        // Button Setting
        Button button_set = (Button) findViewById(R.id.button_setting) ;
        button_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                openSet();
            }
        });
        //Button Score
        Button button_score = (Button) findViewById(R.id.button_score) ;
        button_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                openScore();
            }
        });


    }
    public void openGame() {
        Intent intent = new Intent(this, Game.class);
        startActivity(intent);
    }
    public void openRule(){
        Intent intent = new Intent(this, Rule.class);
        startActivity(intent);
    }
    public void openSet(){
        Intent intent = new Intent(this, Setting.class);
        startActivity(intent);
    }
    public void openScore(){
        Intent intent = new Intent(this, Score.class);
        startActivity(intent);
    }
    /**
     * A native method that is implemented by the 'tetrisbut2048' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();


    private class ToastTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(MainActivity.this, "Toast using timer", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}






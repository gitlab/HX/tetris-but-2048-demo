package com.example.tetrisbut2048;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.SoundPool;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;

public class Rule extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rule);
        SoundPool soundPool = new SoundPool.Builder().build();
        int buttonSound = soundPool.load(this, R.raw.button_sound, 1);
        Button button_back = (Button) findViewById(R.id.button_back);
        button_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                openMain();
            }
        });
    }
    public void openMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
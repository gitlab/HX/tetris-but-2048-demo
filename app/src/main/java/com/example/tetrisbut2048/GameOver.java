package com.example.tetrisbut2048;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Arrays;

public class GameOver extends Rule{
    public static int finalScore = 0;
    protected SharedPreferences sharedPreferences;
    protected ArrayList<String> scoreHistory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gameover);
        TextView show_score = (TextView) findViewById(R.id.show_score);
        show_score.setText("Your Score is: " + finalScore);

        sharedPreferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        scoreHistory = new ArrayList<>();

        Button button_toMain = (Button) findViewById(R.id.button_toMain);
        Button button_save_score = (Button) findViewById(R.id.button_save_score);

        SoundPool soundPool = new SoundPool.Builder().build();
        int buttonSound = soundPool.load(this, R.raw.button_sound, 1);
        button_toMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                openMain();
            }
        });
        button_save_score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundPool.play(buttonSound, 1.0f, 1.0f, 0, 0, 1.0f);
                save_score();
                openScore();
            }
        });
    }
    public void openScore(){
        Intent intent = new Intent(this, Score.class);
        startActivity(intent);
    }
    private void save_score() {
        TextInputEditText textInputEditText = findViewById(R.id.enter_username);
        String username = textInputEditText.getText().toString();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String scoreHistoryString = sharedPreferences.getString("scoreHistory", "");
        if (!scoreHistoryString.isEmpty()) {
            String[] scoreHistoryArray = scoreHistoryString.split(",");
            scoreHistory.addAll(Arrays.asList(scoreHistoryArray));
        }
        scoreHistory.add(username + ": " + finalScore);
        scoreHistoryString = "";
        for (int i = 0; i < scoreHistory.size(); i++) {
            scoreHistoryString += scoreHistory.get(i);
            if (i < scoreHistory.size() - 1) {
                scoreHistoryString += ",";
            }
        }
        editor.putString("scoreHistory", scoreHistoryString);
        editor.apply();
    }
}

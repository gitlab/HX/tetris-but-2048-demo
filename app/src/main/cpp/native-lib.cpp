#include <jni.h>
#include <string>
#include <iostream>

using namespace std;

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_tetrisbut2048_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jint JNICALL
Java_com_example_tetrisbut2048_Game_addToScore(
        JNIEnv* env,
        jobject /* this */,
        jint score,
        jint block1,
        jint block2) {
    cout << "score: " << score << endl;
    cout << "block1: " << block1 << endl;
    cout << "block2: " << block2 << endl;
    int newscore = score + block1 + block2;
    return newscore;
}